import os

// EDIT THIS FOR THE FILES YOU WANT
first_file := "name1.ged"
second_file := "name2.ged"

// Read the lines of the files
// then filter down to just lines with familysearch IDs
// then map down to just IDs
first_file_lines := os.read_lines(first_file) or {
	panic("Error reading first file: " + first_file)
}
first_file_ids := first_file_lines.filter(it.len >= 9 && it[0..9] == "1 _FSFTID").map(it[10..])

second_file_lines := os.read_lines(second_file) or {
	panic("Error reading second file: " + second_file)
}
second_file_ids := second_file_lines.filter(it.len >= 9 && it[0..9] == "1 _FSFTID").map(it[10..])

println("Searching through ${first_file_ids.len} IDs x ${second_file_ids.len} IDs = total space of ${first_file_ids.len * second_file_ids.len} \n")

// Search through all possible pairs of IDs for matches
for first_file_id in first_file_ids {
	for second_file_id in second_file_ids {
		if first_file_id == second_file_id {
			println("First file ID: " + first_file_id + " \t Second file ID: " + second_file_id)
			break
		}
	}
}