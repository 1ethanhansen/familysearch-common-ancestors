# FamilySearch Common Ancestors

A simple tool that digests 2 lists of [FamilySearch](https://www.familysearch.org/en/) IDs and finds common ancestors. Built in [v](https://vlang.io/)

## How to use this tool

In order to use this tool you first need to install [getmyancestors](https://github.com/Linekio/getmyancestors) and use it to download a GEDCOM file of a large number of your ancestors (usually 15+ generations back is where your most common ancestor will be) for each person. Note that this may take a long time - be sure your laptop is plugged in! The recommended naming convention for the files is something like "name1.ged" and "name2.ged".

Now put those GED files into the same directory as `common_ancestor.v`, edit the source code to point to the correct file names, and run `v run common_ancestor.v`. This might take a while because there can be a lot of data to check, but after a while in the terminal you should start getting matching IDs that you can then check on the FamilySearch website.

## The backstory

FamilySearch doesn't have a great way to search for really any ancestors and I had a friend who could also trace her history back pretty far. We wanted to see how closely we were related so I found the getmyancestors tool, built this, and found our we're 10th cousins!